# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Useful resources

- [A Complete Guide to CSS Media Queries](https://css-tricks.com/a-complete-guide-to-css-media-queries/) - This helped me understand media Queries
- [logic in css media queries](https://css-tricks.com/logic-in-css-media-queries/) - This is an amazing article.

## Screenshot

![](./design/desktop-design.jpg)

### Links

[![Netlify Status](https://api.netlify.com/api/v1/badges/8be960b7-d933-49cd-9f4f-859a3f3cbc2d/deploy-status)](https://app.netlify.com/sites/xenodochial-tereshkova-2854e0/deploys)

- Repository: [ solution URL](https://gitlab.com/imad101/qr-code-component)
- Live Site URL: [ live demo ](https://xenodochial-tereshkova-2854e0.netlify.app/)

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow

### What I learned

small things always make deferent

```css
img {
  object-fit: cover / 2;
}
```

### Continued development

I am thinking about convert that item to reuseable QR code component width some frontend framework that can be call whatever we needed.

## Author

- Website - [imadbg01](https://www.github.com/imadbg01)
- Frontend Mentor - [@imadbg01](https://www.frontendmentor.io/profile/imadbg01)
- Twitter - [@ImadBg4](https://www.twitter.com/ImadBg4)

## Acknowledgments

I would like to express my very great appreciation to frontend mentor
